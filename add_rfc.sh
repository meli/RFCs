#!/bin/sh


rfc=
title=
verbose=0

#https://www.rfc-editor.org/info/rfc9393

show_help () {
  printf 'USAGE: %s [-v|--verbose] --rfc RFCXXXX.\n' "${0}" >&2
}

while :; do
    case $1 in
        -h|-\?|--help)   # Call a "show_help" function to display a synopsis, then exit.
            show_help
            exit 0
            ;;
        -n|--rfc)       # Takes an option argument, ensuring it has been specified.
            if [ -n "$2" ]; then
                rfc=$2
                shift
            else
                printf 'ERROR: "--rfc" requires a non-empty option argument.\n' >&2
                exit 1
            fi
            ;;
        --rfc=?*)
            rfc=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --rfc=)         # Handle the case of an empty --rfc=
            printf 'ERROR: "--rfc" requires a non-empty option argument.\n' >&2
            exit 1
            ;;
        -v|--verbose)
            verbose=$((verbose + 1)) # Each -v argument adds 1 to verbosity.
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: If no more options then break out of the loop.
            break
    esac
    shift
done

if [ -z "$rfc" ]; then
  printf 'ERROR: an RFC must be provided.\n' >&2
  exit 1
fi

# add `rfc` prefix if it's missing
case "$rfc" in
  *rfc*) ;;
  *RFC*) ;;
  *) rfc=rfc"$rfc" ;;
esac

#wget "https://www.rfc-editor.org/info/$rfc"
#https://www.rfc-editor.org/rfc/rfc9413.html#
#https://www.rfc-editor.org/rfc/rfc9413.txt


rfc=$(echo "$rfc" | tr '[:upper:]' '[:lower:]')
title=$(curl -s -S "https://datatracker.ietf.org/doc/$rfc/bibtex/" | sed -e 's/^\s*title\s*=\s*[{]*\([^}]*\)[}]*,/\1/p' -e '/^\stitle\s*=/!d')
upperrfc=$(echo "$rfc" | tr '[:lower:]' '[:upper:]')
title=$(echo "$title" | tr '[:space:]' '_')
folder="${upperrfc}_-_${title}"
folder=$(printf "%s" "$folder" | sed -e "s/_*$//" -e "s/\///")

mkdir -p "$folder"
cd "$folder"
wget --quiet "https://www.rfc-editor.org/rfc/$rfc.html"
wget --quiet "https://www.rfc-editor.org/rfc/$rfc.txt"
bzip2 ./*.html
bzip2 ./*.txt
